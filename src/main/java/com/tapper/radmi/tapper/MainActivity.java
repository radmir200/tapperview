package com.tapper.radmi.tapper;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editText;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        editText = (EditText) findViewById(R.id.value_tap);
        button = (Button) findViewById(R.id.to_show);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.to_show) {
            Intent sec = new Intent(MainActivity.this, Tapper.class);
            String str = editText.getText().toString();
            if(str.isEmpty()) str = "0";
            sec.putExtra("val_k", str);
            startActivity(sec);
        }
    }
}
