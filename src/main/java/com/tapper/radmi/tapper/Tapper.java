package com.tapper.radmi.tapper;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.math.BigInteger;
import androidx.appcompat.app.AppCompatActivity;

public class Tapper extends AppCompatActivity {

    WebView wv;
    String val;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tapper);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        Intent intent = getIntent();
        val = (intent.getStringExtra("val_k"));
        wv = (WebView) findViewById(R.id.web_v);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setUseWideViewPort(true);
        BigInteger v = new BigInteger(val);
        v = v.divide(new BigInteger("17"));
        val = v.toString(2);
        while (val.length() < 1802)
            val = "0" + val;


        wv.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                String str_js = "javascript:";
                for(int j=105; j>=0; --j) {
                    for(int i=0; i<17; ++i) {
                        if(val.charAt((105-j)*17+i) != '0') {
                            str_js += "document.getElementById('table').getElementsByTagName('tr')[" +i
                                    + "].getElementsByTagName('td')[" +
                                    j + "].click();\n";
                        }
                    }
                }
                wv.loadUrl(str_js);
            }

            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.cancel();
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });

        wv.loadUrl("file:///android_asset/new.html");
    }
}
